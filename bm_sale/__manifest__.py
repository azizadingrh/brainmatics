# pylint: disable=C0111,W0104
# -*- coding: utf-8 -*-
{
    "name": "Extend Sales",
    "version": "1.0",
    "author": "Brainmatics",
    "website": "http://www.brainmatics.com",
    "category": "sales",
    "depends": ["sale"],
    "description": """
    Custom Quotation \n
    * Create new template called Brainmatics Quotation/Order \n
    * Show Expiration Date in Quotation qweb report \n
    * Hidden 'Payment Terms' field in Quotation form view 
    
v1.0\n

    Custom Sales/Marketing Report (Pivot) \n
    * Add new field Training Implementation Date \n
    * Add group SO based on training implementation month in quotation, SO and sales report \n

v1.1\n
    
author : Aziz Adi N.\n
    """,
    "data": [
        'reports/sale_report_templates.xml',
        'reports/sale_report.xml',
        'views/sale_views.xml',
        ],
    'installable': True,
    'application': True,
    'auto_install': False
}
