# -*- coding: utf-8 -*-
##############################################################################
#
# This module is developed by Aziz Adi Nugroho
# Copyright (C) 2018 Aziz Adi Nugroho
# All Rights Reserved
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from odoo import models, fields


class SaleReport(models.Model):
    _inherit = 'sale.report'

    confirmation_date = fields.Datetime('Confirmation Date', readonly=True)
    training_implementation_date = fields.Datetime('Training Implementation Date', readonly=True)

    def _select(self):
        res = super(SaleReport, self)._select()
        res = res + ', s.confirmation_date as confirmation_date, s.training_implementation_date'
        return res

    def _group_by(self):
        res = super(SaleReport, self)._group_by()
        res = res + ', s.confirmation_date, s.training_implementation_date'
        return res


