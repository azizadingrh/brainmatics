# pylint: disable=C0111,W0104
# -*- coding: utf-8 -*-
{
    "name": "Extend Web",
    "version": "1.0",
    "author": "Brainmatics",
    "website": "http://www.brainmatics.com",
    "category": "web",
    "depends": ["web"],
    "description": """
    Hidden some text in form login (web client)\n
    * Hidden 'Manage Database'\n
    * Hidden 'Powered by Odoo'\n
    \n
    Hidden text in Main Menu (web client)\n
    * Hidden 'Powered by Odoo'\n 
    
v1.0\n
author : Aziz Adi N.\n
    """,
    "data": ['views/webclient_template.xml'],
    'qweb': ['static/src/xml/base.xml'],
    'installable': True,
    'application': True,
    'auto_install': False
}
