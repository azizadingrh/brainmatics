# pylint: disable=C0111,W0104
# -*- coding: utf-8 -*-
{
    "name": "Extend Dates on Sales Order",
    "version": "1.0",
    "author": "Brainmatics",
    "website": "http://www.brainmatics.com",
    "category": "sales",
    "depends": ["sale", "sale_order_dates", "bm_sale"],
    "description": """
    Custom Marketing Training Date \n
    * Display 'Training Implementation Date' on Quotation/SO list view, replace requested_date on view \n
    
v1.0\n
author : Aziz Adi N.\n
    """,
    "data": [
        'views/sale_order_views.xml',
        ],
    'installable': True,
    'application': True,
    'auto_install': False
}
