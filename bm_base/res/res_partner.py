# -*- coding: utf-8 -*-
from odoo import models, fields


class ResPartner(models.Model):
    _inherit = "res.partner"

    pic = fields.Boolean(String='PIC', help="Check this box if this contact is a PIC.")
    student = fields.Boolean(String='Student', help="Check this box if this contact is a PIC.")
