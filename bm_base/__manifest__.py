# pylint: disable=C0111,W0104
# -*- coding: utf-8 -*-
{
    "name": "Extend Base",
    "version": "1.0",
    "author": "Brainmatics",
    "website": "http://www.brainmatics.com",
    "category": "base",
    "depends": ["base"],
    "description": """
    Add Some Fields in Res.Partner Object\n
    * Add 'PIC' field is boolean type\n
    * Add 'Student' field is boolean type\n
    \n
    
v1.0\n
    
    Dashboard to Main Menu\n
    * change the sequence of dashboard menu\n
    \n
    
v1.1\n
author : Aziz Adi N.\n
    """,
    "data": ['res/res_partner_view.xml',
             'base_menu.xml'],
    'installable': True,
    'application': True,
    'auto_install': False
}
