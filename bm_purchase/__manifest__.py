# pylint: disable=C0111,W0104
# -*- coding: utf-8 -*-
{
    "name": "Extend Purchase",
    "version": "1.0",
    "author": "Brainmatics",
    "website": "http://www.brainmatics.com",
    "category": "Purchases",
    "depends": ["purchase"],
    "description": """
    Custom PO \n
    * Create new template called Brainmatics Purchase Order \n
    
v1.0\n
author : Aziz Adi N.\n
    """,
    "data": [
        'reports/purchase_order_templates.xml',
        'reports/purchase_report.xml',
        'views/purchase_view.xml',
        ],
    'installable': True,
    'application': True,
    'auto_install': False
}
