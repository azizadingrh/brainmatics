$(document).ready(function () {
 
    $( ".toggle_leftmenu").click(function() {
    	// hide left menu
            $( ".o_sub_menu").animate({
            width: 'toggle'
        });
    	// animate content left menu
    		$( ".o_sub_menu_content").animate({
            width: 'toggle'
        });
    	// animate company logo	
            $( ".o_sub_menu").find('img').animate({
            width: 'toggle'
        });
    });
    
    $( "ul.nav li a" ).each(function(index) {
        $(this).on("click", function(){
            $( ".o_sub_menu").show();
            $( ".o_sub_menu_content").show();
            $( ".oe_secondary_menu").show();
            $( ".o_sub_menu").find('img').show();
        }); 
    });
});
