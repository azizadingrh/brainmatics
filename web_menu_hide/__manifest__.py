# pylint: disable=C0111,W0104
# -*- coding: utf-8 -*-
{
    "name": "Web Menu Hide/Show",
    "version": "1.0",
    "author": "Brainmatics",
    "website": "http://www.brainmatics.com",
    "category": "Theme",
    "depends": ["web"],
    "description": """
    Hidden Features\n
    * Hidden left menu bar Odoo \n
    
v1.0\n
author : Aziz Adi N.\n
    """,
    "data": ['views/webclient_templates.xml'],
    'installable': True,
    'application': True,
    'bootstrap': True,
    'auto_install': False
}
