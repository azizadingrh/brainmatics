# pylint: disable=C0111,W0104
# -*- coding: utf-8 -*-
{
    "name": "Extend Account Budget",
    "version": "1.0",
    "author": "Brainmatics",
    "website": "http://www.brainmatics.com",
    "category": "Accounting",
    "depends": ["account_budget"],
    "description": """

    Brainmatics Budget Management \n
    * Modify odoo budget management into budget cost (only) management
    in Brainmatics \n

v1.0\n
author : Aziz Adi N.\n

    """,
    "data": [
        'views/account_budget_views.xml',
        ],
    'installable': True,
    'application': True,
    'auto_install': False
}
