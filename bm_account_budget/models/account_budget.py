# -*- coding: utf-8 -*-
##############################################################################
#
# This module is developed by Aziz Adi Nugroho
# Copyright (C) 2018 Aziz Adi Nugroho
# All Rights Reserved
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################


from odoo import models, fields, api
import datetime
from dateutil.relativedelta import *


class CrossoveredBudget(models.Model):
    _inherit = "crossovered.budget"

    bm_tot_planned_amount = fields.Float(compute='_compute_bm_budget', string='Budget Planned', digits=0)
    bm_tot_budget_spent = fields.Float(compute='_compute_bm_budget', string="Budget Spent", digits=0)
    bm_tot_balance = fields.Float(compute='_compute_bm_budget', string="Balance", digits=0)
    bm_tot_percentage = fields.Float(compute='_compute_bm_budget', string="Percentage", digits=0)
    bm_tot_disburse_amount = fields.Float(compute='_compute_bm_budget', string="Disbursed Amount", digits=0,
                                       help="difference between current balance amount and balance amount in the previous month")
    
    @api.multi
    def _compute_bm_budget(self):
        no = 0
        term_budgets = []
        for budgets in self:
            tot_budget_planned = 0.0
            tot_budget_spent = 0.0
            
            for line in budgets.crossovered_budget_line:
                if line.practical_amount <= 0.0:
                    tot_budget_spent += line.practical_amount
                if line.planned_amount <= 0.0:
                    tot_budget_planned += line.planned_amount

            budgets.bm_tot_planned_amount = abs(tot_budget_planned)
            budgets.bm_tot_budget_spent = abs(tot_budget_spent)
            budgets.bm_tot_balance = tot_budget_spent - tot_budget_planned
            tot_percentage = float((budgets.bm_tot_budget_spent or 0.0) / budgets.bm_tot_planned_amount) * 100 \
                             if budgets.bm_tot_planned_amount else 0.0
            budgets.bm_tot_percentage = tot_percentage
            
            date_from = datetime.datetime.strptime(budgets.date_from, '%Y-%m-%d')
            date_to = datetime.datetime.strptime(budgets.date_to, '%Y-%m-%d')
            date_id = str(date_to.year) + str(date_to.month)
            no += 1
            term_budgets.append({
                'ids': budgets,
                'date_id': date_id if date_id else 0,
                'no_id': no,
                'balance': budgets.bm_tot_balance})
        
        # New Calculation based on date range
        for budgets in term_budgets:
            budget_ids = budgets.get('ids')
            no = budgets.get('no_id')
            balance = budgets.get('balance') 
            date_id = budgets.get('date_id')
            previous_month = int(date_id)-1 if date_id > 0 else False
            disburse_amount = 0.0
            if not previous_month:
                disburse_amount = 0.0
            else:
                for previous_budgets in term_budgets:
                    if int(previous_budgets.get('date_id')) == previous_month:
                        disburse_amount = balance - previous_budgets.get('balance')
                        if disburse_amount < 0.0:
                            disburse_amount = 0.0
            budget_ids.bm_tot_disburse_amount = disburse_amount

        # Old Calculation based on sequence no
#         for budgets in term_budgets:
#             print budgets
#             budget_ids = budgets.get('ids') 
#             no = budgets.get('no_id')
#             balance = budgets.get('balance')
#             previous_no = no-1 if no > 0 else False
#             disburse_amount = 0.0
#             if not previous_no:
#                 disburse_amount = 0.0
#             else:
#                 for previous_budgets in term_budgets:
#                     if previous_budgets.get('no_id') == previous_no:
#                         disburse_amount = balance - previous_budgets.get('balance')
#                         if disburse_amount < 0.0:
#                             disburse_amount = 0.0
#             budget_ids.bm_tot_disburse_amount = disburse_amount

class CrossoveredBudgetLines(models.Model):
    _inherit = "crossovered.budget.lines"
    
    bm_planned_amount = fields.Float(compute='_compute_bm_budget_line', string='Budget Planned', digits=0,
                                         help="Brainmatics Budget Planned")
    bm_budget_spent = fields.Float(compute='_compute_bm_budget_line', string="Budget Spent", digits=0,
                                       help="Brainmatics Budget Spent")
    bm_balance = fields.Float(compute='_compute_bm_budget_line', string="Balance", digits=0,
                                       help="Brainmatics Budget Balance")
    bm_percentage = fields.Float(compute='_compute_bm_budget_line', string="Percentage", digits=0,
                                       help="Brainmatics Budget Percentage")
     
    @api.multi
    def _compute_bm_budget_line(self):
        for line in self:
            line.bm_planned_amount = line.planned_amount
            line.bm_budget_spent = line.practical_amount
            if line.planned_amount <= 0.0:
                line.bm_planned_amount = abs(line.planned_amount)
            if line.practical_amount <= 0.0:
                line.bm_budget_spent = abs(line.practical_amount)
            line.bm_balance = line.bm_planned_amount - line.bm_budget_spent
            percentage = float((line.bm_budget_spent or 0.0) / line.bm_planned_amount) * 100 \
                             if line.bm_planned_amount else 0.0
            line.bm_percentage = percentage

