# pylint: disable=C0111,W0104
# -*- coding: utf-8 -*-
{
    "name": "Extend Account",
    "version": "1.0",
    "author": "Brainmatics",
    "website": "http://www.brainmatics.com",
    "category": "Accounting",
    "depends": ["account"],
    "description": """
    Custom Invoice \n
    * Create new template called Brainmatics Invoice \n
    
    Custom Finance Report \n
    * Change template for Finance Report ( Profit & Loss) and Balance Sheet\n
    
    Custom Journal Entries (4 December 2018)\n
    * Change default filter of journal entries from 'Miscellaneous Operations' to 'Cash Operations'

v1.0\n
author : Aziz Adi N.\n

    """,
    "data": [
        'views/report_invoice.xml',
        'views/account_report.xml',
        'views/account_invoice_view.xml',
        'views/report_financial.xml',
        'views/account_view.xml',
        ],
    'installable': True,
    'application': True,
    'auto_install': False
}
