# -*- coding: utf-8 -*-
##############################################################################
#
# This module is developed by Aziz Adi Nugroho
# Copyright (C) 2018 Aziz Adi Nugroho
# All Rights Reserved
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from odoo import models, fields

class AccountInvoice(models.Model):
    _inherit = "account.invoice"
    
    # replaced, for add default text
    term_of_payment = fields.Text('Additional Information', readonly=True, states={'draft': [('readonly', False)]},
                          default='Payment :\n'
                          '1. Transfer Payment to PT. Brainmatics Cipta Informatika, '
                          'BCA Cabang Matraman, Account no. 342.302242.2 \n'  
                          '2. Cash Payment to  Menara Bidakara, Suite 0205, ' 
                          'Jl. Gatot Subroto Kav.71-73,Pancoran-Jakarta 12870.')
