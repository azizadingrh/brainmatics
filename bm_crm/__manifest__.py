# pylint: disable=C0111,W0104
# -*- coding: utf-8 -*-
{
    "name": "Extend CRM",
    "version": "1.0",
    "author": "Brainmatics",
    "website": "http://www.brainmatics.com",
    "category": "CRM",
    "depends": ["crm"],
    "description": """
    Custom Quotation \n
    * Remove 'Next Activity' menu items\n
    * Remove 'Activities' menu in sales report (pivot)
    
v1.0\n
author : Aziz Adi N.\n
    """,
    "data": [
        'views/crm_lead_view.xml'
        ],
    'installable': True,
    'application': True,
    'auto_install': False
}
