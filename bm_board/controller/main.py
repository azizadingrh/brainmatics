# -*- coding: utf-8 -*-
##############################################################################
#
# This module is developed by Aziz Adi Nugroho
# Copyright (C) 2018 Aziz Adi Nugroho
# All Rights Reserved
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from xml.etree import ElementTree

from odoo.http import Controller, route, request
from odoo.addons.board.controllers.main import Board


class InheritBoard(Board):
    
    @route('/board/add_to_dashboard', type='json', auth='user')
    def add_to_dashboard(self, action_id, context_to_save, domain, view_mode, name=''):
        """Replace controller function when add save filter to my dashboard. Make it 
        possible to display on all users"""
        user_obj = request.env['res.users']
        user_ids = user_obj.search([('active', '=', True)]).ids
        # Retrieve the 'My Dashboard' action from its xmlid
        action = request.env.ref('board.open_board_my_dash_action')

        if action and action['res_model'] == 'board.board' and action['views'][0][1] == 'form' and action_id:
            # Maybe should check the content instead of model board.board ?
            view_id = action['views'][0][0]
            board = request.env['board.board'].fields_view_get(view_id, 'form')
            if board and 'arch' in board:
                xml = ElementTree.fromstring(board['arch'])
                column = xml.find('./board/column')
                if column is not None:
                    new_action = ElementTree.Element('action', {
                        'name': str(action_id),
                        'string': name,
                        'view_mode': view_mode,
                        'context': str(context_to_save),
                        'domain': str(domain)
                    })
                    column.insert(0, new_action)
                    arch = ElementTree.tostring(xml, 'utf-8')
                    # custom by Aziz
                    for user_id in user_ids:
                        request.env['ir.ui.view.custom'].sudo().create({
                            'user_id': user_id, # request.session.uid
                            'ref_id': view_id,
                            'arch': arch
                        })
                        
                    return True
        return False