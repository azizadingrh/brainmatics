# pylint: disable=C0111,W0104
# -*- coding: utf-8 -*-
{
    "name": "Extend Board",
    "version": "1.0",
    "author": "Brainmatics",
    "website": "http://www.brainmatics.com",
    "category": "dashboard",
    "depends": ["board"],
    "description": """
    Custom Dashboard Users\n
    * 'Add to my dashboard' can be open to all users, replace add_to_dashboard controller function
    and override field_view_get function from board module\n
    * Hidden button close dashboard
    \n
    
v1.0\n
author : Aziz Adi N.\n
    """,
    "data": [],
    "qweb": ['static/src/xml/board.xml'],
    'installable': True,
    'application': True,
    'auto_install': False
}
